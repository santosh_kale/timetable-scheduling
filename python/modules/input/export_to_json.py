import json

days=['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']

def export_timetable_classwise(timetable):
    attr=timetable.attr
    chrome=timetable.chrome
    out=[]
    out_counter=0
    for yr_index in range(len(chrome)):
        for div_index in range(attr.classes[yr_index]['div']):
            out.append({})
            out[out_counter]['year']=attr.classes[yr_index]['name']
            out[out_counter]['div']=div_index+1
            tt=[]
            for day_index in range(len(chrome[yr_index][div_index])):
                tt.append({})
                tt[day_index]['day']=days[day_index]
                slots=[]
                for slot_index in range(len(chrome[yr_index][div_index][day_index])):
                    slot=chrome[yr_index][div_index][day_index][slot_index]
                    slots.append({})
                    slots[slot_index]['type']='lec' if slot.isLect() else 'prac'
                    slots[slot_index]['resources']=slot.get_names(attr,yr_index)
                tt[day_index]['slots']=slots
            out[out_counter]['timetable']=tt
            out_counter+=1
    return out

def export_timetable_individual(timetable):
    attr=timetable.attr
    chrome=timetable.chrome
    out=[]

    for teacher_index in range(len(attr.teachers)):
        out.append({})
        out[teacher_index]['name']=attr.teachers[teacher_index].name
        tt=[]
        for day_index in range(attr.dimensions[1]):
            tt.append({})
            tt[day_index]['day']=days[day_index]
            slots=[]
            slot_index=0
            slot_count=attr.dimensions[0]
            extra_index=0
            while extra_index<slot_count:
                breaker=False
                slots.append({'resources':{'class':'','subject':'','location':''},'type':'lec'})
                for yr_index in range(len(attr.classes)):
                    for div_index in range(attr.classes[yr_index]['div']):
                        slot = chrome[yr_index][div_index][day_index]
                        if extra_index<slot_count:
                            slot=timetable.get(yr_index,div_index,day_index,extra_index)
                            if extra_index%2!=0 and not slot.isLect():
                                break
                            for tupl in slot.get_names(attr,yr_index):
                                if tupl['teacher']==out[teacher_index]['name']:
                                    slots[slot_index]['resources']={'class':attr.classes[yr_index]['name']+'-'+str(attr.classes[yr_index]['div']),\
                                                                    'subject':tupl['subject'],\
                                                                    'location':tupl['location']}
                                    if slot.isLect():
                                        slots[slot_index]['type'] = 'lec'
                                    elif extra_index%2==0:
                                        slots[slot_index]['type'] = 'prac'
                                        slots[slot_index]['resources']['batch']=tupl['batch']
                                        extra_index+=1
                                    breaker=True
                                    break
                            if breaker:break
                    if breaker: break
                extra_index += 1
                slot_index += 1
            tt[day_index]['slots']=slots
        out[teacher_index]['timetable']=tt
    #print(json.dumps(out, indent=2))
    return out

def export_timetable_classroomwise(timetable):
    attr=timetable.attr
    chrome=timetable.chrome
    out=[]

    for classroom_index in range(len(attr.classrooms)):
        out.append({})
        out[classroom_index]['name']=attr.classrooms[classroom_index]
        tt=[]
        for day_index in range(attr.dimensions[1]):
            tt.append({})
            tt[day_index]['day']=days[day_index]
            slots=[]
            slot_index=0
            slot_count=attr.dimensions[0]
            while slot_index<slot_count:
                slots.append({'resources':{'class':'','subject':'','teacher':''}})
                for yr_index in range(len(attr.classes)):
                    for div_index in range(attr.classes[yr_index]['div']):
                        slot=chrome[yr_index][div_index][day_index]
                        if slot_index<len(slot):
                            slot=slot[slot_index]
                            if slot.isLect():
                                for tupl in slot.get_names(attr,yr_index):
                                    if tupl['location']==out[classroom_index]['name']:
                                        slots[slot_index]['resources']={'class':attr.classes[yr_index]['name']+'-'+str(attr.classes[yr_index]['div']),\
                                                                    'subject':tupl['subject'],\
                                                                    'teacher':tupl['teacher']}
                slot_index += 1
            tt[day_index]['slots']=slots
        out[classroom_index]['timetable']=tt
    return out

def export_timetable_labwise(timetable):
    attr=timetable.attr
    chrome=timetable.chrome
    out=[]

    for lab_index in range(len(attr.labs)):
        out.append({})
        out[lab_index]['name']=attr.labs[lab_index]
        tt=[]
        for day_index in range(attr.dimensions[1]):
            tt.append({})
            tt[day_index]['day']=days[day_index]
            slots=[]
            slot_index=0
            slot_count=attr.dimensions[0]/2
            while slot_index<slot_count:
                slots.append({'resources':{'class':'','batch':'','subject':'','teacher':''}})
                for yr_index in range(len(attr.classes)):
                    for div_index in range(attr.classes[yr_index]['div']):
                        slot = chrome[yr_index][div_index][day_index]
                        if slot_index<len(slot):
                            slot=timetable.get(yr_index,div_index,day_index,slot_index*2)
                            if not slot.isLect():
                                for tupl in slot.get_names(attr,yr_index):
                                    if tupl['location']==out[lab_index]['name']:
                                        slots[slot_index]['resources']={'class':attr.classes[yr_index]['name']+
                                                                        '-'+str(attr.classes[yr_index]['div']),
                                                                        'batch':tupl['batch'],
                                                                        'subject':tupl['subject'],
                                                                        'teacher':tupl['teacher']}
                slot_index += 1
            tt[day_index]['slots']=slots
        out[lab_index]['timetable']=tt
    return out
