import xlsxwriter

path='python\modules\output\\'

days=['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']

header_format=   {'bold':True,'border':2,'align':'center','valign':'vcenter'}
lecture_format=  {'border':1,'align':'center','valign':'vcenter'}
practical_format={'left':1,'right':1,'align':'center','valign':'vcenter'}

def make_lecture(workbook,sheet,row,col,name):
    format=workbook.add_format(lecture_format)
    sheet.merge_range(row,col,row+1,col,name,format)

def make_practical(workbook,sheet,row,col,name):
    sheet.write(row+1,col,name[1],workbook.add_format(practical_format))
    sheet.write(row+2,col,name[2],workbook.add_format(practical_format))
    practical_format['top']=1
    sheet.write(row,col,name[0],workbook.add_format(practical_format))
    practical_format.pop('top')
    practical_format['bottom']=1
    sheet.write(row+3,col,name[3],workbook.add_format(practical_format))
    practical_format.pop('bottom')

def export_classwise_xlsx(name,timetable):
    workbook = xlsxwriter.Workbook(path+name+'.xlsx')
    for t in timetable:
        worksheet = t['year'] + '-' + str(t['div'])
        worksheet = workbook.add_worksheet(worksheet)
        worksheet.set_column(2, 8, 20)
        t=t['timetable']
        for day_index in range(len(t)):
            format=workbook.add_format(header_format)
            worksheet.write(1,2+day_index,t[day_index]['day'],format)
            slot_index=0
            for slot in t[day_index]['slots']:
                if slot!={}:
                    name=[]
                    for resource in slot['resources']:
                        try:
                            name.append(resource['batch']+' : ')
                        except KeyError:
                            name.append('')
                        name[-1]+=resource['subject']
                        name[-1]+=' : '+resource['teacher']+' : '+resource['location']
                    while len(name)!=4:
                        name.append('')
                    if name[0]==' :  : ':
                        name[0]=''
                    if slot['type']=='prac':
                        make_practical(workbook,worksheet,2+slot_index,2+day_index,name)
                        slot_index+=4
                    else:
                        make_lecture(workbook,worksheet,2+slot_index,2+day_index,name[0])
                        slot_index+=2
    workbook.close()

def export_individual_xlsx(sheet_name,timetable):
    workbook = xlsxwriter.Workbook(path+sheet_name+'.xlsx')
    for t in timetable:
        worksheet = t['name']
        worksheet = workbook.add_worksheet(worksheet)
        worksheet.set_column(2, 8, 20)
        t=t['timetable']
        for day_index in range(len(t)):
            format=workbook.add_format(header_format)
            worksheet.write(1,2+day_index,t[day_index]['day'],format)
            slot_index=0
            for slot in t[day_index]['slots']:
                if slot!={}:
                    name=slot['resources']['class']
                    try:
                        name+='-'+slot['resources']['batch']
                    except KeyError:
                        pass
                    name+=' : '+slot['resources']['subject']+' : '+slot['resources']['location']
                    if name==' :  : ':
                        name=''
                    if slot['type']=='prac':
                        format = workbook.add_format(lecture_format)
                        worksheet.merge_range(2+slot_index,2+day_index,5+slot_index,2+day_index, name, format)
                        slot_index+=4
                    else:
                        make_lecture(workbook,worksheet,2+slot_index,2+day_index,name)
                        slot_index+=2
    workbook.close()

def export_classroomwise_xlsx(sheet_name,timetable):
    workbook = xlsxwriter.Workbook(path+sheet_name+'.xlsx')
    for t in timetable:
        worksheet = t['name']
        worksheet = workbook.add_worksheet(worksheet)
        worksheet.set_column(2, 8, 20)
        t=t['timetable']
        for day_index in range(len(t)):
            format=workbook.add_format(header_format)
            worksheet.write(1,2+day_index,t[day_index]['day'],format)
            slot_index=0
            for slot in t[day_index]['slots']:
                if slot!={}:
                    name=slot['resources']['class']+' : '+slot['resources']['subject']+' : '+slot['resources']['teacher']
                    if name==' :  : ':
                        name=''
                    make_lecture(workbook,worksheet,2+slot_index,2+day_index,name)
                    slot_index+=2
    workbook.close()

def export_labwise_xlsx(sheet_name,timetable):
    workbook = xlsxwriter.Workbook(path+sheet_name+'.xlsx')
    for t in timetable:
        worksheet = t['name']
        worksheet = workbook.add_worksheet(worksheet)
        worksheet.set_column(2, 8, 20)
        t=t['timetable']
        for day_index in range(len(t)):
            format=workbook.add_format(header_format)
            worksheet.write(1,2+day_index,t[day_index]['day'],format)
            slot_index=0
            for slot in t[day_index]['slots']:
                if slot!={}:
                    name=slot['resources']['class']+'-'+slot['resources']['batch']+' : '+slot['resources']['subject']+' : '+slot['resources']['teacher']
                    if name=='- :  : ':
                        name=''
                    format = workbook.add_format(lecture_format)
                    worksheet.merge_range(2+slot_index,2+day_index,5+slot_index,2+day_index, name, format)
                    slot_index+=4
    workbook.close()
