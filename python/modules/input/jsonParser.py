'''module implements parsing operations on JSON file to save all 
inputs in python object for further use  
'''

import json
from python.modules.genetic_algorithm.classes import Teacher, Subject

#class that holds all parsing operations for taking input from JSON file

class JsonParser:
    def __init__(self,fileName):
        with open(fileName) as file:
            self.obj=json.load(file)  #loads JSON file with name 'fileName'
    
    #TODO get list of objects of class Teacher which will hold data about teachers
    def getTeachers(self):
        teachers=[]
        for teacher in self.obj['teachers']:
            teachers.append(Teacher(**teacher)) 
        return teachers

    #TODO get list of objects of class Subject which will hold data about theory subjects
    def getSubjects(self):
        subjects=[]
        for subject in self.obj['subjects']:
            subjects.append(Subject(**subject))
        return subjects

    #TODO get list of objects of class Subject which will hold data about practical subjects
    def getPracticals(self):
        practicals=[]
        for practical in self.obj['practicals']:
            practicals.append(Subject(**practical))
        return practicals
    
    #TODO get list of list[subject_index,teacher_index] which will hold data about unique session
    def getSessionLoad(self,teachers,subjects,practicals):
        teachers=list(map(lambda x: x.name,teachers))
        subjects=list(map(lambda x: x.subject,subjects))
        practicals=list(map(lambda x: x.subject,practicals))
        sessionLoadRaw=self.obj['sessionLoad']

        subjectLoad=[]
        practicalLoad=[]
        
        for yr in range(len(self.obj['classes'])):
            subjectLoad.append([])
            practicalLoad.append([])
        
            for div in range(self.obj['classes'][yr]['div']):
                subjectLoad[yr].append([])
                practicalLoad[yr].append([])
                #removed -1 -1 because it has no control of selection
                #subjectLoad[yr][div].append([-1,-1])
                #practicalLoad[yr][div].append([-1,-1])

                for i in sessionLoadRaw:
                    if (i['class']==self.obj['classes'][yr]['name']) and (i['div']== div+1):
                        try:
                            x=i['subject']
                            subjectLoad[yr][div].append([subjects.index(i['subject']),teachers.index(i['teacher'])])
                        except KeyError:
                            practicalLoad[yr][div].append([practicals.index(i['practical']),teachers.index(i['teacher'])])

        return (subjectLoad,practicalLoad) #returns unique lecture sessions and practical sessions in two lists

