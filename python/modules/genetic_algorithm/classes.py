'''
This file holds the definition of all classes that are used to hold data 
'''

import random
import copy
import threading
from python.parameters import EMPTY_PROBABLITY

class Thread(threading.Thread):
    def __init__(self,funcs,arg):
        threading.Thread.__init__(self)
        self.funcs=funcs
        self.arg=arg
    def run(self):
        for individual in self.arg:
            fitness=0
            for func in self.funcs:
                fitness+=func(individual)
            individual.fitness=fitness/(len(self.funcs))

class Attributes:
    def __init__(self,j,constraints):
        self.dimensions=[j.obj['dimensions'].get('rows'),j.obj['dimensions'].get('cols')]
        self.classes=j.obj['classes']
        self.teachers=j.getTeachers()
        self.subjects=j.getSubjects()
        self.practicals=j.getPracticals()
        self.lecSessions,self.pracSessions=j.getSessionLoad(self.teachers,self.subjects,self.practicals)
        self.classrooms=j.obj['classrooms']
        self.labs=j.obj['labs']
        self.constraints=constraints

    def getDimensions(self):
        return (self.dimensions[0],self.dimensions[1])

    def yearIndex(self,year):
        return [clas['name'] for clas in self.classes].index(year)

    def batchIndex(self,year,batch):
        if type(year) is str:
            year=self.yearIndex(year)
        return self.classes[year].get('batches').index(batch)

    def getSession(self,year,div,sessType='lec',count=1,index=-1,empty_probablity=EMPTY_PROBABLITY):
        if type(year) is str:
            year=self.yearIndex(year)
            div=div-1


        if index==-1 and sessType=='lec':
            return random.choice(self.lecSessions[year][div]) if random.random()>empty_probablity else [-1,-1]
        elif index==-1 and sessType=='prac':
            pracs=copy.deepcopy(self.pracSessions[year][div])
            result=[]
            for _ in range(count):
                result.append(random.choice(pracs) if random.random()>empty_probablity else [-1,-1])
                try:
                    pracs.remove(result[-1])
                except ValueError as e:
                    pass
            return result
        if sessType == 'lec':
            return self.lecSessions[year][div][index]
        else:
            return self.pracSessions[year][div][index]


class Teacher:
    def __init__(self,name,theoryLoad,practicalLoad):
        self.name=name
        self.theoryLoad=theoryLoad
        self.practicalLoad=practicalLoad
    def __str__(self):
        return self.name

class Subject:#same for practical
    def __init__(self,subject,weeklyLoad,year):
        self.subject=subject
        self.weeklyLoad=weeklyLoad
        self.year=year
    def __str__(self):
        return self.subject
    

class Slot:
    def __init__(self,session=None,classroom=None,duration=None,copy=None):
        if copy!=None:
            self._copy_construct(copy)
        else:
            self.session=[x[:] for x in session]
            for s,c in zip(self.session,classroom):
                s.append(c)
            
            self.duration=duration
    def getTeachers(self):
        return [sess[1] for sess in  self.session]
    def getSubjects(self):
        return [sess[0] for sess in self.session]
    def getResources(self):
        return [sess[2] for sess in self.session]
    def _copy_construct(self,copy):
        self.session=[x[:] for x in copy.session]
        self.duration=copy.duration

    def isLect(self):
        '''returns True if slot is lecture else false'''
        return self.duration==1

    def isEmpty(self):
        if self.isLect():
            return self.session[0][0]==-1
        else:
            return [sess[0]==-1 for sess in self.session]

    def __str__(self):
        return str(self.session)

    def get_names(self,attr,clas):
        if self.isLect():
            if self.session[0][0]!=-1:
                return [{'subject':str(attr.subjects[self.session[0][0]]),\
                         'teacher':str(attr.teachers[self.session[0][1]]),\
                         'location':attr.classrooms[self.session[0][2]]}]
        else:
            ls=[]
            for session_index in range(len(self.session)):
                s=self.session[session_index]
                if s[0]!=-1:
                    try:
                        ls.append({'batch':attr.classes[clas]['batches'][session_index],\
                                   'subject':str(attr.practicals[s[0]]),\
                                   'teacher':str(attr.teachers[s[1]]),\
                                   'location':attr.labs[s[2]]})
                    except Exception as e:
                        e
            if len(ls)!=0:
                return ls

        return [{'subject': '', 'teacher': '', 'location': ''}]

class Timetable:
    def __init__(self,attr,init=True):
        self.nrow,self.ncol=attr.getDimensions()
        self.chrome=[]
        self.fitness=0.0
        self.soft_fitness=0.0
        self.attr=attr
        self.teacher_conflicts=None
        self.resource_conflicts=None
        if init:
            self._initialize(attr)

    def _initialize(self,attr):
        lecture_sessions=copy.deepcopy(attr.lecSessions)
        practical_sessions=copy.deepcopy(attr.pracSessions)
        classrooms=attr.classrooms
        index=-1
        for yr in range(len(attr.classes)):
            self.chrome.append([])
            for div in range(attr.classes[yr]['div']):
                index+=1
                tt=[]
                for day in range(self.ncol):
                    tt.append([])
                    duration=0
                    while duration<self.nrow:
                        classroom = []
                        session = []
                        labs = attr.labs[:]
                        if random.random()<.33 and len(practical_sessions[yr][div]) >= 1 and duration%2==0:
                            for i in range(len((attr.classes[yr])['batches'])):
                                if len(practical_sessions[yr][div])>=1:
                                    session.append(practical_sessions[yr][div].pop(random.choice(range(len(practical_sessions[yr][div])))))
                                    classroom.append(attr.labs.index(labs.pop(random.choice(range(len(labs))))))
                                else:
                                    session.append([-1,-1])
                                    classroom.append(-1)
                            tt[day].append(Slot(session,classroom,2))
                            duration+=2
                        elif len(lecture_sessions[yr][div])>=1:
                            session.append(lecture_sessions[yr][div].pop(random.choice(range(len(lecture_sessions[yr][div])))))
                            classroom.append(random.choice(range(len(classrooms))))
                            tt[day].append(Slot(session,classroom,1))
                            duration+=1
                        elif duration%2!=0:
                            tt[day].append(Slot([[-1,-1]],[-1],1))
                            duration+=1
                        elif len(practical_sessions[yr][div])==0:
                            for _ in range(len((attr.classes[yr])['batches'])):
                                session.append([-1,-1])
                                classroom.append(-1)
                            tt[day].append(Slot(session,classroom,2))
                            duration+=2

                self.chrome[yr].append(tt)

    def __str__(self):
        string='\n--MasterTimetable---'

        string+='\n\nHard Fitness=>'+str(self.fitness)
        string+='\nSoft Fitness=>'+str(self.soft_fitness)
        line='------------------------------------------------------------------'
        for yr in range(len(self.chrome)):
            for div in range(len(self.chrome[yr])):
                string+='\n\n'+self.attr.classes[yr]['name']+str(div+1)+' ->\n'+line+line
                for day in range(len(self.chrome[yr][div])):
                    string+='\nDay '+str(day+1)
                    for sess in range(len(self.chrome[yr][div][day])):
                        string+='  --  '+self.printSlot(self.chrome[yr][div][day][sess])
        return string

    def __fitness__(self):
        string=''
        string += '\n\nHard Fitness=>' + str(self.fitness)
        string += '\nSoft Fitness=>' + str(self.soft_fitness)
        return string

    def printSlot(self,slot):
        string=''
        if not slot.isLect():
            for s in slot.session:
                if s[0]>=0:
                    string+='('+str(self.attr.practicals[s[0]])+','+str(self.attr.teachers[s[1]])+','+str(self.attr.labs[s[2]])+')'
                else:
                    string+='(EMPTY SLOT)'
        else:
            s=slot.session[0]
            if s[0]>=0:
                string+='('+str(self.attr.subjects[s[0]])+','+str(self.attr.teachers[s[1]])+','+str(self.attr.classrooms[s[2]])+')'
            else:
                string+='(EMPTY SLOT)'
        return string

    def set(self,year,div,day,slot,value):
        tt = self.chrome[year][div]
        count = -1
        for i in range(len(tt[day])):
            count += tt[day][i].duration
            if count >= slot:
                tt[day][i]=value
                return True
        raise Exception('No slot found to set value')

    def get(self,year,div,day,slot):
        tt=self.chrome[year][div]
        count =-1
        for i in range(len(tt[day])):
            count+=tt[day][i].duration
            if count>=slot:
                return tt[day][i]
        raise    Exception('no value found \ninconsistant TT')
