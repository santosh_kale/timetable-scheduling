'''module implements basic oprations like\n 
selection crossover mutation and fitness funtion\n
required to run genetic algorithm 
'''

from python.modules.input import export_to_json as json_export
from python.modules.input import export_to_xlsx as xlsx_export

from python.modules.genetic_algorithm.classes import Timetable,Slot,Thread
import random
import time
import copy
import python.modules.genetic_algorithm.constraints as constraints

from python.parameters import GUIDED_MUTATION

def calc_time(func):

    def function(*args,**kwargs):
        start_time=time.time()
        ret=func(*args,**kwargs)
        end_time=time.time()
        time_taken=end_time-start_time
        time_taken=int(time_taken)
        print('Time taken:',time_taken//60,'min,',time_taken%60,'sec\n')
        return ret
    return function

def _select(population,select_percent,hard_fitness_threshold):
    population.sort(key=lambda x:x.fitness,reverse=True)
    return 0,population[:int(len(population)*select_percent)]

def _cross_over(population,popsize,attr):
    '''crossover: select two parents and perform crossover to create two children
    '''
    offsprings=[]

    while (len(offsprings)<popsize-len(population)):
        parent1=random.choice(population)
        parent2=random.choice(population)

        child1=Timetable(attr,init=False)
        child2=Timetable(attr,init=False)

        #TODO copy parent1 and parent2 in chil1 and child 2
        cross_point=random.randint(0,len(parent1.chrome)-1)
        _copy(parent1,parent2,child1,cross_point,attr)
        _copy(parent2,parent1,child2,cross_point,attr)
        offsprings.append(child1)
        offsprings.append(child2)

    return offsprings

def _swap(individual,lec=[],prac=[]):
    #todo make swap not vary the resources
    if len(lec)==2 and len(prac)==0:

        #swap resorces to compluse them to remain same
        lec[0][-1].session[0][2],lec[1][-1].session[0][2] = lec[1][-1].session[0][2],lec[0][-1].session[0][2]
        #now swap 2 lect
        individual.set(*lec[0][:-1], lec[1][-1])
        individual.set(*lec[1][:-1], lec[0][-1])

    elif len(lec)==2 and len(prac)==1:
        prac=prac[0]
        lec_day=individual.chrome[lec[0][0]][lec[0][1]][lec[0][2]]
        prac_day=individual.chrome[prac[0]][prac[1]][prac[2]]
        lec_day[lec_day.index(lec[0][4])]=prac[4]
        lec_day.pop(lec_day.index(lec[1][4]))
        index=prac_day.index(prac[4])
        prac_day[index]=lec[0][4]
        prac_day.insert(index,lec[1][4])
        add = 0
        for slt in lec_day:
            add += slt.duration
        if add != 6:
            print('err')
            raise Exception('exception add is '+str(add))
        add = 0
        for slt in prac_day:
            add += slt.duration
        if add != 6:
            print('err')
            raise Exception('exception')

    elif len(lec)==0 and len(prac)==2:
        for _ in range(2):
            i = random.randrange(4)
            #first change the resources
            prac[0][-1].session[i][2], prac[1][-1].session[i][2] = prac[1][-1].session[i][2], prac[0][-1].session[i][2]
            #then change actual slot to remain same slots
            prac[0][-1].session[i], prac[1][-1].session[i] = prac[1][-1].session[i], prac[0][-1].session[i]

        '''for i in range(len(prac[0][-1].session)):
            if random.random()>individual.fitness:
                prac[0][-1].session[i], prac[1][-1].session[i]=prac[1][-1].session[i], prac[0][-1].session[i]
        '''


def _swap_resource(individual,lec=None,prac=None):
    if lec != None :
            lec[1][-1].session[0][2],lec[0][-1].session[0][2]=lec[0][-1].session[0][2],lec[1][-1].session[0][2]
    else:
        '''i=random.randrange(4)
        prac[0][-1].session[i][2], prac[1][-1].session[i][2] = prac[1][-1].session[i][2], prac[0][-1].session[i][2]
        '''
        pos1=prac[0][-2]
        pos2=prac[1][-2]
        prac[0][-1].session[pos1][2],prac[1][-1].session[pos2][2]=prac[1][-1].session[pos2][2],prac[0][-1].session[pos1][2]


g_count=[0,0]

def _mutate(population,mutation_rate,attr,guided=True):
    global g_count
    once=False
    def get_teacher_conflicted_slot(individual,yr=None,div=None):
        """returns slot with conflicts else random slot"""
        if yr != None and len(individual.teacher_conflicts[yr][div])>0:
            rday_index,rslot_index=random.choice(individual.teacher_conflicts[yr][div])
            individual.teacher_conflicts[yr][div].remove((rday_index,rslot_index))
        else:
            rday_index = random.randrange(individual.ncol)
            rslot_index = random.randrange(individual.nrow)

        return(rday_index,rslot_index)

    def get_resource_conflicted_slot(individual,yr=None,div=None):
        if yr!=None and len(individual.resource_conflicts[yr][div])!=0:
            rday_index,rslot_index,pos=random.choice(individual.resource_conflicts[yr][div])
            individual.resource_conflicts[yr][div].remove((rday_index,rslot_index,pos))

        else:
            rday_index = random.randrange(individual.ncol)
            rslot_index = random.randrange(individual.nrow)
            pos=random.randrange(4)
        return (rday_index,rslot_index,pos)

    def get_count_conflicted_practical_slot(individual,yr,div,prac=True):
        if prac:
            if len(individual.practical_count_conflict[yr][div])!=0:
                rday_index, rslot_index = random.choice(individual.practical_count_conflict[yr][div])
                individual.practical_count_conflict[yr][div].remove((rday_index, rslot_index))
                return (rday_index,rslot_index)
            elif len(individual.practical_count_conflict[yr][div])==0:
                rday_index = random.randrange(individual.ncol)
                rslot_index = random.randrange(individual.nrow)
                slot = individual.get(yr, div, rday_index, rslot_index)
                while slot.isLect():
                    rday_index = random.randrange(individual.ncol)
                    rslot_index = random.randrange(individual.nrow)
                    slot = individual.get(yr, div, rday_index, rslot_index)
                return (rday_index, rslot_index)

        else:
            #todo get two consicutive lec slots
            rday_index = random.randrange(individual.ncol)
            rslot_index = random.randrange(individual.nrow)
            slot=individual.get(yr,div,rday_index,rslot_index)
            while not slot.isLect():
                rday_index = random.randrange(individual.ncol)
                rslot_index = random.randrange(individual.nrow)
                slot = individual.get(yr, div, rday_index, rslot_index)
            new_slot_index=rslot_index+1 if rslot_index%2==0 else rslot_index-1
            return (rday_index,rslot_index,new_slot_index)

    for individual in population:
        chrome=individual.chrome
        attr=individual.attr

        if guided:
            old_chrome=copy.deepcopy(individual.chrome)

        for yr_index in range(len(attr.classes)):
            for div_index in range(attr.classes[yr_index]['div']):
                for _ in range(len(individual.teacher_conflicts[yr_index][div_index])):
                    day_index,slot_index=get_teacher_conflicted_slot(individual,yr_index,div_index)
                    slot = individual.get(yr_index, div_index, day_index, slot_index)

                    day_index2, slot_index2 = get_teacher_conflicted_slot(individual)
                    slot2 = individual.get(yr_index, div_index, day_index2, slot_index2)

                    if slot.isLect() and slot2.isLect():
                        _swap(individual,lec=[(yr_index,div_index,day_index,slot_index,slot),
                                               (yr_index,div_index,day_index2,slot_index2,slot2)])
                    elif not slot.isLect()and not slot2.isLect():
                        _swap(individual, prac=[(yr_index, div_index, day_index, slot_index, slot),
                                                (yr_index, div_index, day_index2, slot_index2, slot2)])
                    else:
                        lec=(yr_index,div_index,day_index,slot_index,slot) \
                            if slot.isLect() else \
                            (yr_index, div_index, day_index2, slot_index2, slot2)
                        prac=(yr_index,div_index,day_index,slot_index,slot) \
                            if not slot.isLect() else \
                            (yr_index, div_index, day_index2, slot_index2, slot2)
                        new_slot_index=lec[3]+1 if lec[3]%2==0 else lec[3]-1
                        lec2=(yr_index,div_index,lec[2],new_slot_index)
                        new_slot=individual.get(*lec2)
                        lec2+=(new_slot,)
                        _swap(individual,lec=[lec,lec2],prac=[prac])

                no_of_conflicts=len(individual.resource_conflicts[yr_index][div_index])
                for _ in range(no_of_conflicts
                                       if no_of_conflicts>1
                                       else 1):

                    while True:
                        day_index1, slot_index1,pos1 = get_resource_conflicted_slot(individual,yr_index,div_index)
                        slot1 = individual.get(yr_index, div_index, day_index1, slot_index1)

                        if slot1.isLect() and not slot1.isEmpty():
                            while True:
                                day_index2, slot_index2, pos2 = get_resource_conflicted_slot(individual)
                                slot2 = individual.get(yr_index, div_index, day_index2, slot_index2)
                                if slot2.isLect() and not slot2.isEmpty():
                                    _swap_resource(individual, lec=[(yr_index, div_index, day_index1, slot_index1,slot1),
                                                                     (yr_index, div_index, day_index2, slot_index2,slot2)])
                                    break
                            break
                        elif not slot1.isLect():
                            if pos1==None :
                                raise Exception('Practical cannot have None pos')
                            while True:
                                day_index2, slot_index2,pos2 = get_resource_conflicted_slot(individual)
                                slot2 = individual.get(yr_index, div_index, day_index2, slot_index2)
                                if not slot2.isLect():
                                    _swap_resource(individual,prac=[(yr_index, div_index, day_index1, slot_index1,pos1,slot1),
                                                                    (yr_index, div_index, day_index2, slot_index2,pos2,slot2)])

                                    break
                            break

                constraints._fitness_hard_daily_practical_count(individual)

                """for _ in range(len(individual.practical_count_conflict[yr_index][div_index])):
                    prac_day,prac_slot=get_count_conflicted_practical_slot(individual,yr_index,div_index)
                    day_index,slot_index,next_slot_index=get_count_conflicted_practical_slot(individual,yr_index,div_index,False)
                    slot1=individual.get(yr_index,div_index,day_index,slot_index)
                    slot2=individual.get(yr_index,div_index,day_index,next_slot_index)
                    prac=individual.get(yr_index,div_index,prac_day,prac_slot)
                    _swap(individual,lec=[(yr_index,div_index,day_index,slot_index,slot1),
                                         (yr_index,div_index,day_index,next_slot_index,slot2)],
                                    prac=[(yr_index,div_index,prac_day,prac_slot,prac),])

                """

        if guided:
            old_fitness=individual.fitness
            _fitness_one(individual,attr.constraints)
            if old_fitness>individual.fitness:
                individual.chrome=old_chrome
                individual.fitness=old_fitness
                g_count[1]+=1
            else:

                g_count[0]+=1


def _fitness_one(individual,constraints):
    fitness=0
    for constraint in constraints['hard']:
        fitness += constraint(individual)

    individual.fitness = (fitness / len(constraints['hard']))
    fitness = 0
    for constraint in constraints['soft']:
        fitness+=constraint(individual)
    individual.soft_fitness=(fitness/len(constraints['soft']))

def fitness_one(individual,constraints):
    fitness=0
    for constraint in constraints['hard']:
        fitness += constraint(individual)

    individual.fitness = (fitness / len(constraints['hard']))
    fitness = 0
    for constraint in constraints['soft']:
        fitness+=constraint(individual)
    individual.soft_fitness=(fitness/len(constraints['soft']))


def _fitness_function(population,constraints):
    ''' calculate fitness for all constraints
    '''
    args=[
            population[:int(len(population)/4)],
            population[int(len(population)/4):2*int(len(population)/4)],
            population[2*int(len(population)/4):3*int(len(population) / 4)],
            population[3*int(len(population) / 4):]
         ]
    threads=[]
    for arg in args:
        thread=Thread(constraints['hard'], arg)
        thread.run()
        threads.append(thread)

    for individual in population:
        fitness=0
        for constraint in constraints['soft']:
            fitness+=constraint(individual)
        individual.soft_fitness=(fitness/len(constraints['soft']))

    for thread in threads:
        'thread.join()'


class GeneticAlgorithm:
    def __init__(self, *args, **kwargs):
        self.fitness_hard = 0.0
        self.fitness_soft = 0.0
        self.gen_no = 0
        self.args = args
        self.kwargs = kwargs
        self.stop_var=False
    def run(self):
        return self.genetic_algorithm(*self.args, **self.kwargs)

    def get_fitness(self):
        return self.fitness_hard, self.fitness_soft, self.gen_no

    def stop(self):
        self.stop_var=True
    @calc_time
    def genetic_algorithm(self, popsize, generations, mutation_rate, select_percent, attr):
        '''carry out genetic algorithm using other methods
        '''
        population = [Timetable(attr) for x in range(popsize)]
        _fitness_function(population, attr.constraints)
        prev_fitness=-1
        hard_fitness_threshold = 0

        for generation in range(generations):

            if self.stop_var:
                return
            hard_fitness_threshold, population = _select(population, select_percent, hard_fitness_threshold)
            offsprings = _cross_over(population, popsize, attr)
            _fitness_function(offsprings,attr.constraints)
            _fitness_function(population,attr.constraints)
            _mutate(offsprings, mutation_rate, attr, guided=GUIDED_MUTATION)
            _mutate(population,mutation_rate,attr,guided=True)

            offsprings.extend(population)
            population = offsprings

            if not GUIDED_MUTATION:
                _fitness_function(offsprings, attr.constraints)

            population.sort(key=lambda x: x.fitness, reverse=True)
            print('Generation=>', generation)
            print('restored', g_count[1], ' updated', g_count[0])
            printHard(population[0], attr.constraints['hard'])

            print(population[0].__fitness__())


            self.fitness_hard = population[0].fitness
            self.fitness_soft = population[0].soft_fitness
            self.gen_no = generation

        print(population[0])
        _fitness_one(population[0], attr.constraints)
        print(population[0])
        printHard(population[0], attr.constraints['hard'])

        xlsx_export.export_classwise_xlsx('Classwise', json_export.export_timetable_classwise(population[0]))
        xlsx_export.export_individual_xlsx('Individual', json_export.export_timetable_individual(population[0]))
        xlsx_export.export_classroomwise_xlsx('Classroomwise', json_export.export_timetable_classroomwise(population[0]))
        xlsx_export.export_labwise_xlsx('Labwise', json_export.export_timetable_labwise(population[0]))

        return population[0]


def _copy(parent1, parent2, child, cross_point, attr):
    '''used to copy parent1 and parent2 genes after cross_point into child
       first parent1 is copied then parent2
    '''

    chrome = []
    index = -1
    for yr in range(len(attr.classes)):
        chrome.append([])
        for div in range(attr.classes[yr]['div']):
            tt = []
            if index < cross_point:
                for day_index in range(parent1.ncol):
                    tt.append([])
                    for slot_index in range(len(parent1.chrome[yr][div][day_index])):
                        tt[day_index].append(Slot(copy=parent1.chrome[yr][div][day_index][slot_index]))

            else:
                for day_index in range(parent2.ncol):
                    tt.append([])
                    for slot_index in range(len(parent2.chrome[yr][div][day_index])):
                        tt[day_index].append(Slot(copy=parent2.chrome[yr][div][day_index][slot_index]))

            chrome[yr].append(tt)
    child.chrome = chrome


def printFit(pop, mess='fitness'):
    ''' print the fittest individual for display on cmd
    '''
    print(mess, '=>', end='')
    for ind in pop:
        print(ind.fitness, end=' ')
    print('')


def printHard(ind, hard):
    for cons in hard:
        print(cons.__name__, '\t\t', cons(ind))