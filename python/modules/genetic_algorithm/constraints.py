"""holds all constraints as functions who return some fitness value
"""
from python.modules.genetic_algorithm.classes import Timetable


def _fitness_hard_daily_practical_count(individual):
    attr=individual.attr
    total_fitness=fitness=0
    individual.practical_count_conflict=[[[] for _ in range(attr.classes[i]['div'])] for i in range(len(attr.classes))]
    for yr in range(len(attr.classes)):
        for div in range(attr.classes[yr]['div']):
            for day_index in range(individual.ncol):
                practical_count=0
                forgive_flag=False
                for slot_index in range(individual.nrow//2):
                    slot = individual.get(yr, div, day_index, slot_index*2)
                    if not slot.isLect():
                        practical_count+=1
                        if practical_count>1:
                            individual.practical_count_conflict[yr][div]\
                                .append((day_index,slot_index*2))
                    elif slot_index==individual.nrow//4:
                        forgive_flag=True

                if practical_count==0:continue
                total_fitness+=2
                #if practical_count==1: #or forgive_flag==True:
                #    fitness+=1
                fitness+=3-practical_count
    return (fitness/total_fitness)


def _fitness_hard_teacher_conflict(individual):
    total_fitness = 0
    fitness = 0
    attr = individual.attr
    count_teacher = [0 for _ in range(len(attr.teachers))]
    individual.teacher_conflicts=[[[] for _ in range(attr.classes[i]['div'])] for i in range(len(attr.classes))]
    for day_index in range(individual.ncol):
        for slot_index in range(individual.nrow):

            for i in range(len(attr.teachers)):
                count_teacher[i] = 0

            for yr in range(len(attr.classes)):
                for div in range(attr.classes[yr]['div']):
                    slot = individual.get(yr, div, day_index, slot_index)
                    for teacher_index in slot.getTeachers():
                        if teacher_index >= 0:
                            count_teacher[teacher_index] += 1
                            if count_teacher[teacher_index]>1:
                                individual.teacher_conflicts[yr][div].append((day_index, slot_index))
            for counts in count_teacher:
                if counts == 0: continue
                total_fitness += 1
                if counts == 1:
                    fitness += 1


    return (fitness / total_fitness)


def _fitness_hard_resource_conflict(individual):

    total_fitness = 0
    fitness=0
    attr=individual.attr
    individual.resource_conflicts=[[[] for _ in range(attr.classes[i]['div'])] for i in range(len(attr.classes))]
    count_classrooms=[0 for _ in range(len(attr.classrooms))]
    count_labs=[0 for _ in range(len(attr.labs))]

    for day_index in range(individual.ncol):
        for slot_index in range(individual.nrow):
            
            for i in range(len(attr.classrooms)):
                count_classrooms[i]=0
            for i in range(len(attr.labs)):
                count_labs[i]=0

            for yr in range(len(attr.classes)):
                for div in range(attr.classes[yr]['div']):
                    slot=individual.get(yr,div,day_index,slot_index)

                    if slot.isLect():
                        index = slot.getResources()[0]
                        if index != -1:
                            count_classrooms[index] += 1
                            if count_classrooms[index]>1:
                                individual.resource_conflicts[yr][div].append((day_index, slot_index,0))

                    else:
                        for lab_index,i in zip(slot.getResources(), range(len(slot.getResources()))):
                            if lab_index>=0:
                                count_labs[lab_index]+=1
                                if count_labs[lab_index]>1:
                                    individual.resource_conflicts[yr][div].append((day_index, slot_index,i))

            for counts in count_classrooms:
                if counts==0 : continue
                total_fitness+=1
                if counts==1:   fitness+=1

            for counts in count_labs:
                if counts==0: continue
                total_fitness+=1
                if counts==1:   fitness+=1

    return (fitness/total_fitness)


def _fitness_hard_weekly_load(individual):
    
    total_fitness=0
    fitness=0
    attr= individual.attr; chrome=individual.chrome
    count_teacher_theory=[0 for _ in range(len(attr.teachers))]#for all TTs
    count_teacher_practical=[0 for _ in range(len(attr.teachers))]#for all TTs
    count_subject=[0 for _ in range(len(attr.subjects))]#for particular TT
    count_practical=[0 for _ in range(len(attr.practicals))]#for particular TT
    years=['SE','TE','BE']
    for yr in range(len(attr.classes)):
        for div in range(attr.classes[yr]['div']):

            for i in range(len(count_subject)):
                count_subject[i]=0
            for i in range(len(count_practical)):
                count_practical[i]=0

            for day_index in range(len(chrome[yr][div])):
                for slot_index in range(len(chrome[yr][div][day_index])):
                    slot=chrome[yr][div][day_index][slot_index]
                    if slot.isLect():
                        for teacher_index in slot.getTeachers():
                            if teacher_index>=0:
                                count_teacher_theory[teacher_index]+=1
                        for subject_index in slot.getSubjects():
                            if subject_index>=0:
                                count_subject[subject_index]+=1
                    else:
                        for teacher_index in slot.getTeachers():
                            if teacher_index>=0:
                                count_teacher_practical[teacher_index]+=2
                        for practical_index in slot.getSubjects():
                            if practical_index>=0:
                                count_practical[practical_index]+=2

            for i in range(len(count_subject)):
                if attr.subjects[i].year!=years[yr]:continue
                total_fitness+=attr.subjects[i].weeklyLoad
                #if attr.subjects[i].weeklyLoad==count_subject[i]:fitness+=1
                fitness+=(attr.subjects[i].weeklyLoad-abs(attr.subjects[i].weeklyLoad-count_subject[i]))
                fitness
            for i in range(len(count_practical)):
                if attr.practicals[i].year!=years[yr]:continue
                total_fitness+=attr.practicals[i].weeklyLoad
                #if attr.practicals[i].weeklyLoad==count_practical[i]: fitness+=1
                fitness+=(attr.practicals[i].weeklyLoad-abs(attr.practicals[i].weeklyLoad-count_practical[i]))
                fitness
    #for whole teacher load
    for i in range(len(count_teacher_theory)):
        total_fitness+=attr.teachers[i].theoryLoad+attr.teachers[i].practicalLoad
        fitness+=attr.teachers[i].theoryLoad-abs(attr.teachers[i].theoryLoad-count_teacher_theory[i])
        fitness+=attr.teachers[i].practicalLoad-abs(attr.teachers[i].practicalLoad-count_teacher_practical[i])

    return (fitness/total_fitness)


def _fitness_hard_subject_dailyload(individual):
    total_fitness=0
    fitness=0
    chrome=individual.chrome

    for yr in range(len(chrome)):
        for div in range(len(chrome[yr])):
            for day in range(len(chrome[yr][div])):
                subjectCount={}
                for slot in chrome[yr][div][day]:
                    for subject_index in slot.getSubjects():
                        if subject_index!=-1:
                            if not slot.isLect():
                                #subject_index+=50
                                continue
                            try:
                                subjectCount[subject_index]+=1
                            except KeyError:
                                subjectCount[subject_index]=1
                            
                '''total_fitness+=len(subjectCount)*2
                for count in subjectCount.values():
                    if(count<=2):
                        fitness+=3-count
                '''
                total_fitness+=len(subjectCount)
                for count in subjectCount.values():
                    if count<3:
                        fitness+=1
    return (fitness/total_fitness)

'''
def _fitness_hard_discrete_practicals(individual):
    total_fitness = 0
    fitness = 0
    attr = individual.attr
    #count=[0 for _ in range(individual.nrow*individual.ncol)]
    for day_index in range(individual.ncol):
        for slot_index in range(individual.nrow):
            count=0
            for yr in range(len(attr.classes)):
                for div in range(attr.classes[yr]['div']):
                    slot = individual.get(yr, div, day_index, slot_index)
                    if not slot.isLect():
                        for teacher in slot.getTeachers():
                            if teacher!=-1:
                                count+=1

            if count == 0: continue
            total_fitness += 1
            if count < len(attr.labs):
                fitness += 1


    return (fitness / total_fitness)

'''

def _fitness_hard_empty_slot_position(individual):
    total_fitness=0
    fitness=0
    chrome=individual.chrome
    for yr in range(len(chrome)):
        for div in range(len(chrome[yr])):
            for day in range(len(chrome[yr][div])):
                flag=True
                first_empty_count=0
                last_empty_count=0
                for slot in chrome[yr][div][day]:
                    condition=slot.isEmpty()
                    condition= True in condition if type(condition)==list else condition
                    if condition and flag:
                        first_empty_count+=1
                        total_fitness+=1
                    elif condition:
                        last_empty_count+=1
                        total_fitness+=1
                    if not condition:
                        flag=False
                        last_empty_count=0

                fitness+=first_empty_count+last_empty_count

    return fitness/total_fitness


def _fitness_soft_teacher_dailyload(individual):
    total_fitness=0
    attr= individual.attr; chrome=individual.chrome

    fitness=0

    for day in range(individual.ncol):
        count_teacher_day=[0 for _ in range(len(attr.teachers))] #for all TTs
        for yr in range(len(attr.classes)):
            for div in range(attr.classes[yr]['div']):
                for slot in chrome[yr][div][day]:
                    for teacher_index in slot.getTeachers():
                        if teacher_index>=0:
                            if slot.isLect():
                                count_teacher_day[teacher_index]+=1
                            else:
                                count_teacher_day[teacher_index]+=2
        for count in count_teacher_day:
            if count>0:
                total_fitness+=1
            if count>0 and count<=4:
                fitness+=1
    
    return (fitness/total_fitness)*100

def get_constraints():
    constraints={}
    constraints['hard']=[]
    constraints['soft']=[]
    functions=globals()
    for key in functions:
        if key.find('hard')!=-1:
            constraints['hard'].append(functions[key])
        if key.find('soft')!=-1:
            constraints['soft'].append(functions[key])
    return constraints
    
