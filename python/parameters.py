"""
    defines parameters of the algorithm
"""

POPULATION_SIZE=200
GENERATIONS=4
MUTATION_RATE=None       #will not affect due to new method
SELECT_PERCENT=.2
GUIDED_MUTATION=False   #{True|False}// affectd .02 of fitness
EMPTY_PROBABLITY=.12    #will not affect due to new method
