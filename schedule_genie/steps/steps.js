const {ipcRenderer} = require('electron');
var child_process = require('child_process');
const dialog = require('electron').remote.dialog;
var fs = require("fs");
let url = 'http://localhost:5000/';
let interval;
let fitness = {}
var proc;
var softChart,hardChart;
ipcRenderer.on('kill-server',(event,param) => {
    
    $.ajax({
        url : url+'shutdown',
        success : function(res){
            console.log(res)
            proc.kill('SIGINT');
            alert('killed process before closing');
        },
        error : function(err){
            console.log(err)
        }
    })
})

initBars();

$("#manual-input").on('click',function(event){
    ipcRenderer.send('manual-input')
})
$("#home").on('click',function(){
	ipcRenderer.send('home');
})
$(".step-btn").on('click',function(ev){
	$(".step-btn").each(function(index)
	{
		$(this).removeClass('active');
	})
	$(ev.currentTarget).addClass('active');

    $(".a-step").each(function(){
        $(this).hide();
    })
    if(ev.currentTarget.id == 'input')
    {
        //console.log('show input')
        $("#step-input").show();
    }
    else if(ev.currentTarget.id == 'process')
    {
        //console.log('show process')
        $("#step-process").show();
    }
    else if(ev.currentTarget.id == 'output')
    {
        //console.log('show output')
        $("#step-output").show();
    }
})
$(".an-input").hover(function(event)
{
	$(".an-input").each(function(index)
	{
		$(this).addClass("half-opaque");
	})
	$(event.currentTarget).removeClass("half-opaque");
  console.log(event.currentTarget.id)
  if(event.currentTarget.id == 'sample-template')
  {
    $("#excel-input").removeClass("half-opaque")
  }
})
$(".an-input").mouseleave(function(event)
{
	$(".an-input").each(function(index)
	{
		$(this).removeClass("half-opaque");
	})
})
$("#btn-run-algo").on('click',function(){
    startPythonServer();
})
$("#download-all").on('click',function(){
    $(".xls").each(function(ev)
    {
        //console.log($(this));
        $(this)[0].click()
    })
})
$("#rerun").on('click',function(){
	stopProcess();
	clearInterval(interval);
	proc.kill('SIGINT');    
	updateChart({'hard':0,'soft':0,'gen_no':0});
	$("#chart-wrap").hide();
    $("#gen-no-wrap").hide();
    $("#btn-run-algo").show();
})

interval=null;

function startPythonServer()
{

    proc = child_process.spawn('python',['..\\pythonServer.py'])
    proc.stdout.on('data',(data) => {
        //console.log(data.toString())
    })
    proc.stderr.on('data',(data)=>{
        //console.log(data.toString())
        if(data.toString().includes("Running"))
        {
            $("#btn-run-algo").hide();
            showBars();
            runGenAlgo();
            interval = setInterval(function(){
                getFitness()
            },5000); 
        }
    })
    proc.on('close',(code)=>
    {
        //console.log(code)
    })
	
}
function runGenAlgo()
{
    //console.log('sending request')
    $.ajax({
        url : url+'run',
        success : function(res){
            console.log('Gen Algo is running : ',res)
            $("#total-generations").text(res);
        },
        error : function(err)
        {
            console.log('error : ',err)
        }
    })  
}
function getFitness()
{
    $.ajax({
        url : url+'fitness',
        success : function(res){
            fitness = JSON.parse(res);
            if(!fitness.isAlive)
            {
                clearInterval(interval)
                /*alert('Interval cleared')*/
            }
            let x = ((fitness.hard*100))
            x = x.toFixed(0);
            fitness.hard = parseInt(x);
            let y = ((fitness.soft))
            y = y.toFixed(0);
            fitness.soft = parseInt(y);
            
			updateChart(fitness);
        },
        error : function(err)
        {
            console.log(err)
        }
    })
}

function showBars()
{
    $("#chart-wrap").show()
    $("#gen-no-wrap").show()
}
function initBars()
{
    var sChartOptions = {
        type: 'doughnut',
        data: {
            labels: ["Fitness", "Unfit"],
            datasets: [
                {
                    data: [70,30],
                    backgroundColor: ["#5AD3D1","lightgrey","limegreen"],
                    hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
                }
            ]
        },
        options: {
            responsive: false,
            animation : {
                duration : 5000
            },
            title:{
                display:true,
                text: 'Hard Constraints'
            },
        }    
    };
    var hChartOptions = {
        type: 'doughnut',
        data: {
            labels: ["Fitness", "Unfit"],
            datasets: [
                {
                    data: [70,30],
                    backgroundColor: ["#5AD3D1","lightgrey","limegreen"],
                    hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
                }
            ]
        },
        options: {
            responsive: false,
            animation : {
                duration : 5000
            },
            title:{
                display:true,
                text: 'Soft Constraints'
            },
        }    
    };
    var ctxDS = document.getElementById("soft-chart").getContext('2d');
    softChart = new Chart(ctxDS, sChartOptions);

    var ctxDH = document.getElementById("hard-chart").getContext('2d');
    hardChart = new Chart(ctxDH, hChartOptions);
}
function updateChart(fitness)
{
    console.log(fitness);
    softChart.data.labels.pop();
    softChart.data.labels.pop();
    softChart.data.datasets.forEach((dataset) => {
        dataset.data.pop();
        dataset.data.pop();
    });

    softChart.data.labels.push("Fitness");
    softChart.data.labels.push("Unfit");
    softChart.data.datasets.forEach((dataset) => {
        dataset.data.push(fitness.hard);
        dataset.data.push(100-fitness.hard);
    });
    softChart.update();

    hardChart.data.labels.pop();
    hardChart.data.labels.pop();
    hardChart.data.datasets.forEach((dataset) => {
        dataset.data.pop();
        dataset.data.pop();
    });

    hardChart.data.labels.push("Fitness");
    hardChart.data.labels.push("Unfit");
    hardChart.data.datasets.forEach((dataset) => {
        dataset.data.push(fitness.soft);
        dataset.data.push(100-fitness.soft);
    });
    hardChart.update();

    $("#gen-no-val").text(fitness.gen_no+1)
    $("#hard-fitness-val").text(fitness.hard)
}
function convertExcelToJSON(file)
{
  var url = file;
  var res;
  var oReq = new XMLHttpRequest();
  oReq.open("GET", url, true);
  oReq.responseType = "arraybuffer";

  oReq.onload = function(e) {
    var arraybuffer = oReq.response;

    /* convert data to binary string */
    var data = new Uint8Array(arraybuffer);
    var arr = new Array();
    for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
    var bstr = arr.join("");

    var workbook = XLSX.read(bstr, {type:"binary"});
    currentWorkbook = workbook;
    var first_sheet_name = workbook.SheetNames[0];
    var worksheet = workbook.Sheets[first_sheet_name];
    
    fs.writeFile("./assets/json-files/teacher-load-dis.json",JSON.stringify((XLSX.utils.sheet_to_json(worksheet,{raw:true}))),(err) => {
       if(err)
      {
        console.err(err);
        return;
      };
      dialog.showMessageBox({
        message : "JSON File created",
        buttons : ["OK"]
      });
    })
  }

  oReq.send();
}

$("#excel-input").on("click",function(eve)
  {
    let file = dialog.showOpenDialog();
    if(file != undefined)
    {
      convertExcelToJSON(file); 
      var rows;
      fs.readFile("./assets/json-files/teacher-load-dis.json", (err, data) => {
        if (err) throw err;

        excelData = (JSON.parse(data.toString()));
        console.log('excelData as Object : ')
        console.log(excelData);

        var finalJSON = {};
        var rows = 6;
        var cols = 5;
        finalJSON.dimensions = {
          "rows" : rows,
          "cols" : cols
        }
        finalJSON.classes = readClasses();
        finalJSON.teachers = readTeachers(excelData);
        finalJSON.sessionLoad = readSessionLoad(finalJSON,excelData);
		finalJSON.sessionLength=finalJSON.sessionLoad.length
        finalJSON.labs = readLabs()
        finalJSON.classrooms = readClassrooms()
        mergeSubsPracs(finalJSON);
        //console.log(finalJSON);
        trimFinalJson(finalJSON);

        //console.log(finalJSON);
        fs.writeFile("../python/modules/input/input.json",JSON.stringify(finalJSON), function(err) {
            if(err) {
                return console.log(err);
            }

           // console.log("The file was saved!");
            fs.readFile("../python/modules/input/input.json",(err,data) => {
                console.log(JSON.parse(data.toString()));
                /*mark input completed*/
                /*$("#input").addClass('complete');
                $("#process").addClass('complete');
                $("#output").addClass('complete');*/
            });
        });
      });
    }
  });

function readClasses()
{
    let worksheet = currentWorkbook.Sheets[(currentWorkbook.SheetNames[0])];
    //console.log(worksheet);
    var classes_in = ['C1','D1','E1','F1','G1','H1'];
    var classes = [];
    var added = [];
    for(let i=0;i<classes_in.length;i++)
    {
      //console.log('error for i = ',i)
      var a_class = ((((worksheet[classes_in[i]]).v).replace(/\s/g,'')).split("-")[0]);
      if(added.includes(a_class))
      {
          for(let j=0;j<classes.length;j++)
          {
              if(classes[j].name == a_class)
                  classes[j].div = classes[j].div+1; 
          }  
      }
      else
      {
        classes.push({ name : a_class , div : 1 , batches : ["A","B","C","D"]}); 
        added.push(a_class); 
      }
      
    }
    return classes;
}
function readTeachers(excelData)
{
    var teachers = [];
    for(let i=0;i<excelData.length;i++)
      teachers.push({name : (excelData[i].Staff),theoryLoad : (excelData[i].TH),practicalLoad : (excelData[i].PR)})
    return teachers;
}
function readSessionLoad(finalJSON,excelData)
{
    let sessionLoad = [];
    let worksheet = currentWorkbook.Sheets[(currentWorkbook.SheetNames[0])];
    /*cols which represent columns in load-dis.xls*/
    let cols = ['C','D','E','F','G','H'];

    finalJSON.practicals = [];
    finalJSON.subjects = [];

    for(let i=0;i<cols.length;i++)
    {
        var class_;
        var div_;
        /*console.log(excelData,excelData.length);*/
        for(let j=0;j<=excelData.length;j++)
        {
            let tempObj = {};
            if(j+1 == 1)// here we get the class with division
            {
               /* console.log('for cell : ',(worksheet[cols[i]+(j+1)].v).split("-")[1]);*/
                class_ = (worksheet[cols[i]+(j+1)].v).split("-")[0].trim("");
                div_ = parseInt((worksheet[cols[i]+(j+1)].v).split("-")[1].trim(""));
            }
            else if(worksheet[cols[i]+(j+1)] != undefined)
            {
              /*console.log('for spltting based on P or T',(worksheet[cols[i]+(j+1)].v).split(","));*/
              temparr = (worksheet[cols[i]+(j+1)].v).split(",");
              //console.log(temparr);
              if(temparr[0].includes("["))
              {
                /*console.log('before replacing : ',temparr[0])*/
                temparr[0]=temparr[0].replace("[","");
                /*console.log('replaced',temparr[0]);*/
              }
              if(temparr.length == 1)
              {
                  /*here, worksheet[cols[i]+(j+1)] can
                    be a subject and/or prac,
                    detected by P or T
                  */
                  let x = ((temparr[0].split("-"))[((temparr[0].split("-").length)-1)][0]);

                  let weekload;
                  if(temparr[0].indexOf('(')!=-1)
                  {
                    weekload = parseInt(temparr[0].substring((temparr[0].indexOf('(')+1),(temparr[0].lastIndexOf('-'))));//.substr((temparr[0].indexOf('C')),2);
                  }
                  tempObj = {};
                  tempObj.class = class_;
                  tempObj.div = div_;
                  if(x == "P")
                  {
                    finalJSON.practicals.push({year : class_ , subject : temparr[0] , weeklyLoad : weekload ,div : div_});
                    tempObj.practical = temparr[0];
					weekload=weekload/2
                  }
                  else if(x == "T")
                  {
                    finalJSON.subjects.push({year : class_ , subject : temparr[0] , weeklyLoad : weekload ,div : div_});
                    tempObj.subject = temparr[0];
                  }
                  else if(x == "F")// FE teacher
                  {
                    tempObj.subject = temparr[0];
                  }
                  tempObj.teacher = (worksheet[('B'+(j+1))].v);
                  for(var ix=0;ix<weekload;ix++)
					sessionLoad.push(tempObj); 
              }
              else if(temparr.length == 2)
              {
                 let weekload = parseInt(temparr[0].substring((temparr[0].indexOf('(')+1),(temparr[0].indexOf(')'))));
                 tempObj = {};
                 tempObj.class = class_;
                 tempObj.div = div_;
                 tempObj.subject = temparr[0];
                 finalJSON.subjects.push({year : class_ , subject : temparr[0] , weeklyLoad : weekload });
                 tempObj.teacher = (worksheet[('B'+(j+1))].v);
                 for(var ix=0;ix<weekload;ix++)
					sessionLoad.push(tempObj);
                 
                 weekload = parseInt(temparr[1].substring((temparr[1].indexOf('(')+1),(temparr[1].indexOf(')'))));
                 tempObj = {};
                 tempObj.class = class_;
                 tempObj.div = div_;
                 tempObj.practical = temparr[1];
                 finalJSON.practicals.push({year : class_ , subject : temparr[1] , weeklyLoad : weekload});
                 tempObj.teacher = (worksheet[('B'+(j+1))].v);
                 for(var ix=0;ix<weekload/2;ix++)
					sessionLoad.push(tempObj);
              }
            }
        }
    }
  return sessionLoad;
}

function mergeSubsPracs(finalJSON)
{
    let prac_names=[];
    let new_practicals = [];
    for(let i=0;i<finalJSON.practicals.length;i++)
    {
      finalJSON.practicals[i].subject = ((finalJSON.practicals[i].subject).split("(")[0]).trim("");  
      finalJSON.practicals[i].weeklyLoad = parseInt(finalJSON.practicals[i].weeklyLoad);
      
      if(!prac_names.includes(finalJSON.practicals[i].subject))
        prac_names.push(finalJSON.practicals[i].subject);
    }
    for(let i=0;i<finalJSON.subjects.length;i++)
    {
        finalJSON.subjects[i].subject = ((finalJSON.subjects[i].subject).split("(")[0]).trim("");
    }

    for(let i=0;i<prac_names.length;i++)
    {
        temp_arr = $.grep(finalJSON.practicals, function (element, index) {
            return element.subject == prac_names[i];
        });
        total_weeklyLoad=0;
        for (var j=0;j<temp_arr.length;j++) 
        {
            total_weeklyLoad += temp_arr[j].weeklyLoad;
        }
        if(temp_arr[0] != undefined)
        {
          temp_arr[0].weeklyLoad = (total_weeklyLoad/2);
          delete temp_arr[0].div;
          new_practicals.push(temp_arr[0]);
        }
    }
    delete finalJSON.practicals;
    finalJSON.practicals = new_practicals;
}
function trimFinalJson(finalJSON)
{
  for(let i=0;i<finalJSON.sessionLoad.length;i++)
  {

    if(finalJSON.sessionLoad[i].subject == undefined) //it is a prac
    {
      if(finalJSON.sessionLoad[i].practical != undefined)
      finalJSON.sessionLoad[i].practical = (((finalJSON.sessionLoad[i].practical).split("("))[0]).trim("");
    }
    else
    {
      finalJSON.sessionLoad[i].subject = (((finalJSON.sessionLoad[i].subject).split("("))[0]).trim("");  
    }
    /*finalJSON.sessionLoad[i].div = numsOfRoman[finalJSON.sessionLoad[i].div];*/
  }
  //patches
  patchNoDivSubjects(finalJSON);
}
function patchNoDivSubjects(finalJSON)
{
    for(let i=0;i<finalJSON.subjects.length;i++)
    {
        let x = finalJSON.subjects[i];
        for(let j=i+1;j<finalJSON.subjects.length;j++)
        {
            let y = finalJSON.subjects[j];
            if(x.subject == y.subject && x.weeklyLoad == y.weeklyLoad && x.year == y.year)
            {
                finalJSON.subjects.splice(j,1);
                delete x.div;
                break;
            }
        }   
    }
}

/*--------------------------NIRAJ CHANGES-----------------------*/

function readClassrooms()
{
    let worksheet = currentWorkbook.Sheets[(currentWorkbook.SheetNames[0])];

	var classes=[]
	var j=2
	while(worksheet["M"+j] != undefined){
		classes.push((worksheet["M"+j]).v)
		j++
	}
	
    return classes;
}

function readLabs()
{
    let worksheet = currentWorkbook.Sheets[(currentWorkbook.SheetNames[0])];

	var labs=[]
	var j=2
	while(worksheet["N"+j] != undefined){
		labs.push((worksheet["N"+j]).v)
		j++
	}
	
    return labs;
}

function stopProcess(){
    console.log('sending request')
    $.ajax({
        url : url+'stop',
        success : function(res){
            console.log('Stopped the current run: ',res)
			
        },
        error : function(err)
        {
            console.log('error : ',err)
        }
})}