const {ipcRenderer} = require('electron');

$(document).ready(function()
{
	$("body").css("background","ghostwhite")
})
$("#steps").on('click',function(){
	console.log('clicked steps')
    ipcRenderer.send('steps')
})
$("#home").on('click',function(){
	console.log('clicked steps')
	ipcRenderer.send('home');
})
$(".add-teacher").on('click',function(){
	console.log('Added teacher count :');
	console.log($(".a-teacher").css('display'))
	let teachersAdded = $(".a-teacher").length;
	//let new_teacher = ($(".a-teacher")[0])
	let new_teacher = "<div class='a-teacher'><div class='an-attr'>	<div class='teacher-name label'>Teacher Name</div><input type='text' name='' class='teacher-name-val val-text'></div><div class='remove-teacher-wrap right'><button class='remove-teacher'>Remove Teacher</button></div><hr><div class='an-attr'> <div class='a-class'> <div class='class-year label'>Class</div> <select class='class-select'> <option>SE</option> <option>TE</option> <option>BE</option> </select> <div class='an-attr'> <div class='label transparent'>hidden</div> <div class='label division-text'> Division <input type='number' name='' size='2' max='9' min='1' value='1' class='division-num'> </div> <div class='label transparent'>hidden</div> <div class='label division-text'> <div class='vertical-middle inline-block'>Subjects</div> <textarea class='subjects-area vertical-middle' rows='3' cols='30'></textarea> </div> </div> </div> <div class='an-attr right'> <button class='add-class'>Add Class</button> </div> </div> <hr> <div class='an-attr'> <div class='label theory-load'>Theory Load</div> <input type='number' name='' class='val-text theory-load-val'> </div> <div class='an-attr'> <div class='label theory-load'>Practical Load</div> <input type='number' name='' class='val-text prac-load-val'> </div></div>";
	if(teachersAdded == 1 && $(".a-teacher").css('display') == 'none')
	{
		$(".a-teacher").css('display','inline-block')	
	}
	else
	{
		$(".teacher-wrap").append("<br>")
		$(".teacher-wrap").append(new_teacher)
	}
})
$(document).on("click",".add-class",function(ev){
	let clicked = $(ev.currentTarget)[0]
	console.log()

	let new_class = "<br><div class='a-class'> <div class='class-year label'>Class</div> <select class='class-select'> <option>SE</option> <option>TE</option> <option>BE</option> </select> <div class='an-attr'> <div class='label transparent'>hidden</div> <div class='label division-text'> Division <input type='number' name='' size='2' max='9' min='1' value='1' class='division-num'> </div> <div class='label transparent'>hidden</div> <div class='label division-text'> <div class='vertical-middle inline-block'>Subjects</div> <textarea class='subjects-area vertical-middle' rows='3' cols='30'></textarea> </div> </div> <div class='remove-class-wrap right'><button class='remove-class'>Remove Class</button></div></div> <hr>";
	$(this).parent().parent().prepend(new_class)
})
$(document).on("click",".remove-teacher",function(ev){
	$(this).parent().parent().remove()
})
$(document).on("click",".remove-class",function(ev){
	$(this).parent().parent().remove()
})
$(document).on("click",".add-cr",function(ev){
	let cr = "<div class='an-attr'> <div class='crs-wrap'> <div class='a-cr'><input type='text' name='' class='val-text'><div class='remove-cr-wrap inline-block'><button class='remove-cr'>Remove Classroom</button></div> </div></div> </div>";
	$(this).parent().parent().prepend(cr)
})
$(document).on("click",".add-lab",function(ev){
	let lab = "<div class='an-attr'> <div class='labs-wrap'><div class='a-lab'> <input type='text' name='' class='val-text'><div class='remove-lab-wrap inline-block'><button class='remove-lab'>Remove Lab</button></div> </div> </div> </div>";
	$(this).parent().parent().prepend(lab)
})
$(document).on("click",".remove-cr",function(ev){
	$(this).parent().parent().remove()
})
$(document).on("click",".remove-lab",function(ev){
	$(this).parent().parent().remove()
})

