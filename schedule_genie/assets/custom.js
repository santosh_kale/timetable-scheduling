var pat6h = require('path');
const ipc = require('electron').ipcRenderer;
var sys = require('util');
var exec = require('child_process').exec;
var fs = require("fs");
const notifier = require('node-notifier')
const dialog = require('electron').remote.dialog;
window.$ = window.jQuery = require('jquery');

var child;
var currentWorkbook;

var numsOfRoman = {"I" : 1,"II" : 2,"III" : 3,"IV" : 4,"V" : 5,"VI" : 6,"VII" : 7,"VIII" : 8,"IX" : 9,"X" : 10};
function launchPython(evt)
{  
  if(evt.srcElement.id == "interact")
  {
      child = exec("python -i assets/pythonExample.py assets/jsonData.json", function (error, stdout, stderr) {
      
      console.log(error,stdout,stderr);
      if (error !== null) {
        console.log('exec error: ' + error);
      }0
    });
    child.stdout.on('data', function(data) {
        data = data.replace(/'/g,'"');
        console.log(JSON.parse(data),typeof(JSON.parse(data)),JSON.parse(data).length);
        if(typeof(JSON.parse(data)) == "object" && JSON.parse(data).length)
        {
            dialog.showMessageBox({
              message : "Data Received from Python",
              buttons : ["OK"]
            });
        }
      });
      child.stdin.write("exit");
      child.stdin.end();
  }
  else
  {

  }

}
function convertExcelToJSON(file,currentTarget,task)
{
  var url = file;
  var res;
  var oReq = new XMLHttpRequest();
  oReq.open("GET", url, true);
  oReq.responseType = "arraybuffer";

  oReq.onload = function(e) {
    var arraybuffer = oReq.response;

    /* convert data to binary string */
    var data = new Uint8Array(arraybuffer);
    var arr = new Array();
    for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
    var bstr = arr.join("");

    var workbook = XLSX.read(bstr, {type:"binary"});
    currentWorkbook = workbook;
    var first_sheet_name = workbook.SheetNames[0];
    var worksheet = workbook.Sheets[first_sheet_name];
    
    fs.writeFile("./assets/json-files/"+task+".json",JSON.stringify((XLSX.utils.sheet_to_json(worksheet,{raw:true}))),(err) => {
       if(err)
      {
        console.err(err);
        return;
      };
      dialog.showMessageBox({
        message : "JSON File created",
        buttons : ["OK"]
      });
      $(currentTarget).find("img").attr("src","assets/images/completed.jpg");
    })
  }

  oReq.send();
}
function change(menu)
{
  menu.classList.toggle('change');
}
document.addEventListener('DOMContentLoaded', function() 
{
  $(".a-task").on("click",function(eve)
    {
      let task = $(eve.currentTarget).attr("data");    
      let file = dialog.showOpenDialog();
      if(file != undefined)
      {
        if(task== "load-distr")
        {
          convertExcelToJSON(file,$(eve.currentTarget),task); 
          var rows;
          fs.readFile("assets/json-files/"+task+".json", (err, data) => {
            if (err) throw err;
            excelData = (JSON.parse(data.toString()));
            //console.log(excelData);

            var finalJSON = {};
            var rows = 6;
            var cols = 5;
            finalJSON.dimensions = {
              "rows" : rows,
              "cols" : cols
            }
            finalJSON.classes = readClasses();
            finalJSON.teachers = readTeachers(excelData);
            finalJSON.sessionLoad = readSessionLoad(finalJSON,excelData);
            mergeSubsPracs(finalJSON);
            //console.log(finalJSON);
            trimFinalJson(finalJSON);

            //console.log(finalJSON);
            fs.writeFile("assets/json-files/finalJSON.json",JSON.stringify(finalJSON), function(err) {
                if(err) {
                    return console.log(err);
                }

                console.log("The file was saved!");
                fs.readFile("assets/json-files/finalJSON.json",(err,data) => {
                    console.log(JSON.parse(data.toString()));
                });
            });
          });
        } 
      }
    });
})

function readClasses()
{
    let worksheet = currentWorkbook.Sheets[(currentWorkbook.SheetNames[0])];
    console.log(worksheet);
    var classes_in = ['C1','D1','E1','F1','G1','H1'];
    var classes = [];
    var added = [];
    for(let i=0;i<classes_in.length;i++)
    {
      console.log('error for i = ',i)
      var a_class = ((((worksheet[classes_in[i]]).v).replace(/\s/g,'')).split("-")[0]);
      if(added.includes(a_class))
      {
          for(let j=0;j<classes.length;j++)
          {
              if(classes[j].name == a_class)
                  classes[j].div = classes[j].div+1; 
          }  
      }
      else
      {
        classes.push({ name : a_class , div : 1 , batches : ["A","B","C","D"]}); 
        added.push(a_class); 
      }
      
    }
    return classes;
}
function readTeachers(excelData)
{
    var teachers = [];
    for(let i=0;i<excelData.length;i++)
      teachers.push({name : (excelData[i].Staff),theoryLoad : (excelData[i].TH),practicalLoad : (excelData[i].PR)})
    return teachers;
}
function readSessionLoad(finalJSON,excelData)
{
    let sessionLoad = [];
    let worksheet = currentWorkbook.Sheets[(currentWorkbook.SheetNames[0])];
    /*cols which represent columns in load-dis.xls*/
    let cols = ['C','D','E','F','G','H'];

    finalJSON.practicals = [];
    finalJSON.subjects = [];

    for(let i=0;i<cols.length;i++)
    {
        var class_;
        var div_;

        console.log(excelData,excelData.length);
        for(let j=0;j<excelData.length;j++)
        {
            let tempObj = {};
            if(j+1 == 1)// here we get the class with division
            {
               /* console.log('for cell : ',(worksheet[cols[i]+(j+1)].v).split("-")[1]);*/
                class_ = (worksheet[cols[i]+(j+1)].v).split("-")[0].trim("");
                div_ = parseInt((worksheet[cols[i]+(j+1)].v).split("-")[1].trim(""));
            }
            else if(worksheet[cols[i]+(j+1)] != undefined)
            {
              /*console.log('for spltting based on P or T',(worksheet[cols[i]+(j+1)].v).split(","));*/
              temparr = (worksheet[cols[i]+(j+1)].v).split(",");
              if(temparr[0].includes("["))
              {
                console.log('before replacing : ',temparr[0])
                temparr[0]=temparr[0].replace("[","");
                console.log('replaced',temparr[0]);
              }
              if(temparr.length == 1)
              {
                  /*here, worksheet[cols[i]+(j+1)] can
                    be a subject and/or prac,
                    detected by P or T
                  */
                  let x = ((temparr[0].split("-"))[((temparr[0].split("-").length)-1)][0]);

                  let weekload;
                  if(temparr[0].indexOf('(')!=-1)
                  {
                    weekload = parseInt(temparr[0].substring((temparr[0].indexOf('(')+1),(temparr[0].lastIndexOf('-'))));//.substr((temparr[0].indexOf('C')),2);
                  }
                  tempObj = {};
                  tempObj.class = class_;
                  //tempObj.div = div_;
                  if(x == "P")
                  {
                    finalJSON.practicals.push({year : class_ , subject : temparr[0] , weeklyLoad : weekload });
                    tempObj.practical = temparr[0];
                  }
                  else if(x == "T")
                  {
                    finalJSON.subjects.push({year : class_ , subject : temparr[0] , weeklyLoad : weekload });
                    tempObj.subject = temparr[0];
                  }
                  else if(x == "F")// FE teacher
                  {
                    tempObj.subject = temparr[0];
                  }
                  tempObj.teacher = (worksheet[('B'+(j+1))].v);
   				  sessionLoad.push(tempObj); 
              }
              else if(temparr.length == 2)
              {
                 let weekload = parseInt(temparr[0].substring((temparr[0].indexOf('(')+1),(temparr[0].indexOf(')'))));
                 tempObj = {};
                 tempObj.class = class_;
                 tempObj.div = div_;
                 tempObj.subject = temparr[0];
                 finalJSON.subjects.push({year : class_ , subject : temparr[0] , weeklyLoad : weekload ,div : div_});
                 tempObj.teacher = (worksheet[('B'+(j+1))].v);
				 sessionLoad.push(tempObj);
                 
                 weekload = parseInt(temparr[1].substring((temparr[1].indexOf('(')+1),(temparr[1].indexOf(')'))));
                 tempObj = {};
                 tempObj.class = class_;
                 tempObj.div = div_;
                 tempObj.practical = temparr[1];
                 finalJSON.practicals.push({year : class_ , subject : temparr[1] , weeklyLoad : weekload ,div : div_});
                 tempObj.teacher = (worksheet[('B'+(j+1))].v);
  				 sessionLoad.push(tempObj);
              }
            }
        }
    }
  return sessionLoad;
}

function mergeSubsPracs(finalJSON)
{
    let prac_names=[];
    let new_practicals = [];
    for(let i=0;i<finalJSON.practicals.length;i++)
    {
      finalJSON.practicals[i].subject = ((finalJSON.practicals[i].subject).split("(")[0]).trim("");  
      finalJSON.practicals[i].weeklyLoad = parseInt(finalJSON.practicals[i].weeklyLoad);
      
      if(!prac_names.includes(finalJSON.practicals[i].subject))
        prac_names.push(finalJSON.practicals[i].subject);
    }
    for(let i=0;i<finalJSON.subjects.length;i++)
    {
        finalJSON.subjects[i].subject = ((finalJSON.subjects[i].subject).split("(")[0]).trim("");
    }

    for(let i=0;i<prac_names.length;i++)
    {
        temp_arr = $.grep(finalJSON.practicals, function (element, index) {
            return element.subject == prac_names[i];
        });
        total_weeklyLoad=0;
        for (var j=0;j<temp_arr.length;j++) 
        {
            total_weeklyLoad += temp_arr[j].weeklyLoad;
        }
        if(temp_arr[0] != undefined)
        {
          temp_arr[0].weeklyLoad = (total_weeklyLoad/2);
          delete temp_arr[0].div;
          new_practicals.push(temp_arr[0]);
        }
    }
    delete finalJSON.practicals;
    finalJSON.practicals = new_practicals;
}
function trimFinalJson(finalJSON)
{
  for(let i=0;i<finalJSON.sessionLoad.length;i++)
  {
    if(finalJSON.sessionLoad[i].subject == undefined) //it is a prac
    {
      finalJSON.sessionLoad[i].practical = (((finalJSON.sessionLoad[i].practical).split("("))[0]).trim("");
    }
    else
    {
      finalJSON.sessionLoad[i].subject = (((finalJSON.sessionLoad[i].subject).split("("))[0]).trim("");  
    }
    finalJSON.sessionLoad[i].div = numsOfRoman[finalJSON.sessionLoad[i].div];
  }
  //patches
  patchNoDivSubjects(finalJSON);
}
function patchNoDivSubjects(finalJSON)
{
    for(let i=0;i<finalJSON.subjects.length;i++)
    {
        let x = finalJSON.subjects[i];
        for(let j=i+1;j<finalJSON.subjects.length;j++)
        {
            let y = finalJSON.subjects[j];
            if(x.subject == y.subject && x.weeklyLoad == y.weeklyLoad && x.year == y.year)
            {
                finalJSON.subjects.splice(j,1);
                delete x.div;
                break;
            }
        }   
    }
}
