const {app, BrowserWindow ,ipcMain} = require('electron');
var ui;
app.on('window-all-closed', () => {
  app.quit()
})

app.on('ready', () => {
	const {screen} = require('electron');
	var screenSize = screen.getPrimaryDisplay().size;
	ui = new BrowserWindow({
		height: screenSize.height,
		width: screenSize.width,
		resizable: true,
		icon : 'assets/images/icon.png'
	});
	ui.loadURL('file://' + __dirname + '/steps/steps.html');
	ui.setMenu(null);
	ui.maximize();
	ui.on('closed', () => {
  		app.quit()
	})
	/*ui.on('close',function() {
		ui.webContents.send('kill-server');
	})*/



});
ipcMain.on('steps',(event,param) => {
	ui.loadURL('file://' + __dirname + '/steps/steps.html');
	ui.openDevTools();
});
ipcMain.on('home',(event,param) => {
	ui.loadURL('file://' + __dirname + '/home/home.html');
});
ipcMain.on('manual-input',(event,param) => {
	ui.loadURL('file://'+ __dirname + '/custom-input/custom-input.html')
})
