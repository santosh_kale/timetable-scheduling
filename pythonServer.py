from python.parameters import *
from main import GeneticAlgorithmThread
from flask import Flask

from python.modules.input.jsonParser import JsonParser
from python.modules.genetic_algorithm.classes import Attributes
from python.modules.genetic_algorithm.constraints import get_constraints

import json

app = Flask(__name__)
genetic_algo_obj = None

@app.route("/")
def main():
    return "connected"

@app.route("/fitness")
def get_fitness():
	global genetic_algo_obj
	ret={}
	ret["hard"],ret["soft"],ret["gen_no"]=genetic_algo_obj.get_fitness()
	ret["isAlive"] = genetic_algo_obj.isAlive()
	ret = json.dumps(ret)
	return ret.replace("'",'"')

@app.route('/stop')
def stop_algo():
	global genetic_algo_obj
	genetic_algo_obj.stop()
	genetic_algo_obj=None
	return "Stopped successfuly"
@app.route("/run")
def run_algo():
	global genetic_algo_obj

	if genetic_algo_obj != None and genetic_algo_obj.isAlive():
		pass
	else :
		attr=Attributes(JsonParser("..\\python\\modules\\input\\input.json"),get_constraints())
		genetic_algo_obj=GeneticAlgorithmThread(popsize=POPULATION_SIZE,generations=GENERATIONS,mutation_rate=MUTATION_RATE, select_percent=SELECT_PERCENT,attr=attr)
		genetic_algo_obj.start()
	return str(GENERATIONS)

if __name__ == "__main__":
    app.run(debug=True)