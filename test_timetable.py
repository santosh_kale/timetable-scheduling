import unittest
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

from python.modules.input.jsonParser import JsonParser
from python.modules.genetic_algorithm.classes import Attributes
from python.modules.genetic_algorithm.constraints import get_constraints
import python.modules.genetic_algorithm.genetic_algorithm as ga
from python.parameters import *
class TestTimetable(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        attr = Attributes(JsonParser('python\modules\input\input.json'), get_constraints())
        cls.tt =ga.GeneticAlgorithm(popsize=POPULATION_SIZE,generations=GENERATIONS,mutation_rate=MUTATION_RATE\
                                   , select_percent=SELECT_PERCENT,attr=attr)
        cls.tt=cls.tt.run()

    def test_teacher_resource_allocation(self):
        self.assertIsNotNone(self.__class__.tt)
        individual=self.__class__.tt
        exception=False
        attr = individual.attr
        count_teacher = {}
        count_classrooms = {}
        count_labs = {}
        for day_index in range(individual.ncol):
            for slot_index in range(individual.nrow):

                for i in range(len(attr.teachers)):
                    count_teacher[i] = 0
                for i in range(len(attr.classrooms)):
                    count_classrooms[i] = 0
                for i in range(len(attr.labs)):
                    count_labs[i] = 0

                for yr in range(len(attr.classes)):
                    for div in range(attr.classes[yr]['div']):
                        slot = individual.get(yr, div, day_index, slot_index)
                        for teacher_index in slot.getTeachers():
                            if teacher_index >= 0:
                                count_teacher[teacher_index] += 1

                        if slot.isLect():
                            if slot.getTeachers()[0] != -1:
                                for classroom_index in slot.getResources():
                                    count_classrooms[classroom_index] += 1

                        else:
                            for teacher_index, lab_index in zip(slot.getTeachers(), slot.getResources()):
                                if teacher_index >= 0 and lab_index >= 0:
                                    count_labs[lab_index] += 1
                chooser=0
                try:
                    chooser=1
                    for i in range(len(count_teacher.values())):
                        if count_teacher[i] == 0: continue
                        self.assertEquals(count_teacher[i],1)
                    chooser=2
                    for i in range(len(count_classrooms.values())):
                        if count_classrooms[i] == 0: continue
                        self.assertEquals(count_classrooms[i],1)
                    chooser=3
                    for i in range(len(count_labs.values())):
                        if count_labs[i] == 0: continue
                        self.assertEquals(count_labs[i],1)
                except AssertionError as e:
                    exception=True
                    print('day_index',day_index)
                    print('solt_index',slot_index)
                    if chooser==1:print('teacher',attr.teachers[i])
                    elif chooser==2:print('classroom',attr.classrooms[i])
                    else:print('lab',attr.labs[i])
                    print(e)
        if exception:
            raise AssertionError('resource_allocation')


    def test_teacher_hard_weekly_load(self):
        self.assertIsNotNone(self.__class__.tt)
        individual=self.__class__.tt
        exception=False
        total_fitness = 0
        fitness = 0
        attr = individual.attr;
        chrome = individual.chrome
        count_teacher_theory = [0 for _ in range(len(attr.teachers))]  # for all TTs
        count_teacher_practical = [0 for _ in range(len(attr.teachers))]  # for all TTs
        count_subject = [0 for _ in range(len(attr.subjects))]  # for perticlular TT
        count_practical = [0 for _ in range(len(attr.practicals))]  # for perticlular TT
        years = ['SE', 'TE', 'BE']
        for yr in range(len(attr.classes)):
            for div in range(attr.classes[yr]['div']):

                for i in range(len(count_subject)):
                    count_subject[i] = 0
                for i in range(len(count_practical)):
                    count_practical[i] = 0

                for day_index in range(len(chrome[yr][div])):
                    for slot_index in range(len(chrome[yr][div][day_index])):
                        slot = chrome[yr][div][day_index][slot_index]
                        if slot.isLect():
                            for teacher_index in slot.getTeachers():
                                if teacher_index >= 0:
                                    count_teacher_theory[teacher_index] += 1
                            for subject_index in slot.getSubjects():
                                if subject_index >= 0:
                                    count_subject[subject_index] += 1
                        else:
                            for teacher_index in slot.getTeachers():
                                if teacher_index >= 0:
                                    count_teacher_practical[teacher_index] += 2
                            for practical_index in slot.getSubjects():
                                if practical_index >= 0:
                                    count_practical[practical_index] += 2
                subj=None

                try:
                    subj=True
                    for i in range(len(count_subject)):
                        if attr.subjects[i].year != years[yr]: continue
                        self.assertEqual(attr.subjects[i].weeklyLoad,count_subject[i])
                    subj=False
                    for i in range(len(count_practical)):
                        if attr.practicals[i].year != years[yr]: continue
                        self.assertEqual(attr.practicals[i].weeklyLoad,count_practical[i])
                except AssertionError as e:
                    exception=True
                    print('yr',yr)
                    print('div',div)
                    print('subject',attr.subjects[i]) if subj else print('practical',attr.practicals[i])
                    print(e)
        if exception :
            raise AssertionError('teacher_weekly_load')
        # for whole teacher load


    def test_fitness_hard_subject_dailyload(self):
        self.assertIsNotNone(self.__class__.tt)
        individual = self.__class__.tt
        fitness = 0
        chrome = individual.chrome
        exception=False
        for yr in range(len(chrome)):
            for div in range(len(chrome[yr])):
                for day in range(len(chrome[yr][div])):
                    subjectCount = {}
                    for slot in chrome[yr][div][day]:
                        for subject_index in slot.getSubjects():
                            if subject_index != -1:
                                if not slot.isLect():
                                    # subject_index+=50
                                    continue
                                try:
                                    subjectCount[subject_index] += 1
                                except KeyError:
                                    subjectCount[subject_index] = 1
                    try:
                        for count in subjectCount.values():
                            self.assertLess(count,3)
                    except AssertionError as e:
                        exception=True
                        print('yr',yr)
                        print('div',div)
                        print('day',day)
                        print(e)
        if exception:
            raise Exception('daily_load')


    def test_fitnesss_hard_descreate_practicals(self):
        individual = self.__class__.tt
        attr = individual.attr
        exception=False
        for day_index in range(individual.ncol):
            for slot_index in range(individual.nrow):
                count=0
                for yr in range(len(attr.classes)):
                    for div in range(attr.classes[yr]['div']):
                        slot = individual.get(yr, div, day_index, slot_index)
                        if not slot.isLect():
                            for teacher in slot.getTeachers():
                                if teacher!=-1:
                                    count+=1

                if count == 0: continue
                try:self.assertLess(count,len(attr.labs))
                except AssertionError as e:
                    exception = True
                    print('day_index', day_index)
                    print('solt_index', slot_index)
                    print(e)
        if exception:
            raise AssertionError('discreate practicals')


if __name__=='__main__':
    unittest.main()
