from python.modules.input.jsonParser import JsonParser
from python.modules.genetic_algorithm.classes import Attributes
from python.modules.genetic_algorithm.constraints import get_constraints
from python.modules.genetic_algorithm.genetic_algorithm import GeneticAlgorithm
from python.parameters import *
from threading import Thread


class GeneticAlgorithmThread(Thread) :
	def __init__(self,*args,**kwargs) :
		Thread.__init__(self)
		self.gen_obj = GeneticAlgorithm(*args,**kwargs)
	def run(self):
		self.gen_obj.run()
	def get_fitness(self):

		return self.gen_obj.get_fitness()
	def stop(self):
		self.gen_obj.stop()



#Main that invokes different modules in their sequence
if __name__=='__main__':
    attr = Attributes(JsonParser('python\modules\input\input.json'), get_constraints())
    timetable = GeneticAlgorithm( popsize=POPULATION_SIZE,
								  generations=GENERATIONS,
								  mutation_rate=MUTATION_RATE,
								  select_percent=SELECT_PERCENT,
								  attr=attr )
    timetable = timetable.run()
